package com.example.ais.repository;

import com.example.ais.model.tariff;
import com.example.ais.model.tariffCountCash;
import com.example.ais.model.tariffCountTrip;
//import com.example.ais.repositoryImpl.AllTariff;
import com.example.ais.model.tariffDelay;
import com.example.ais.repositoryImpl.AllTariff;
import org.apache.tomcat.jni.User;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.HashMap;


//@Repository
//@Qualifier(value = "tariff")
public interface TariffRepo {
//
//    String username = "postgres";
//    public User findByUsername (String username);
//
//    /**Общее количество билетов*/
//    @Query(nativeQuery = true, value="SELECT count(*) FROM tariff")
//    Integer countAll();
//}

    void create();

    void delete();

    AllTariff get(int idTicket);

    tariffCountCash getCountCash(int idTicket);

    tariffCountTrip getCountTrip(int idTicket);

    tariffDelay getDelay(int idTicket);

    HashMap<Integer, AllTariff> getAll();


}