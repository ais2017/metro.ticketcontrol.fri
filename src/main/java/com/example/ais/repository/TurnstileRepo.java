package com.example.ais.repository;

        import com.example.ais.model.turnstile;

        import java.util.Collection;
        import java.util.HashMap;

public interface TurnstileRepo {

    turnstile create();

    void delete();

    turnstile get(int id);

    HashMap<Integer, turnstile> getAll();
}