package com.example.ais.dao;

import com.example.ais.model.tariff;
import com.example.ais.model.tariffCountCash;
import com.example.ais.model.tariffCountTrip;
import com.example.ais.model.tariffDelay;

import java.util.ArrayList;
import java.util.Date;

public class forBPMNRepository {

    private String tariff;
    private int idTicket;
    private int chislo;
    private Date valid;

    public forBPMNRepository(String tariff, int idTicket, int chislo, Date valid) {
        this.tariff = tariff;
        this.idTicket = idTicket;
        this.chislo = chislo;
        this.valid = valid;
    }

    public String getTariff() {
        return tariff;
    }

    public int getIdTicket() {
        return idTicket;
    }

    public int getChislo() {
        return chislo;
    }

    public Date getValid() {
        return valid;
    }
    //public ArrayList<forBPMNRepository> listTickets = new ArrayList<>();

    public static ArrayList<forBPMNRepository> generateBPMNList(){
        Date dd = new Date(1016,12, 17, 17, 0);
        ArrayList<forBPMNRepository> listTickets = new ArrayList<>();
        forBPMNRepository a = new forBPMNRepository("CountCash", 1,90,dd);
        listTickets.add(a);
        a = new forBPMNRepository("CountTrip", 3,30,dd);
        listTickets.add(a);
        a = new forBPMNRepository("Delay", 5,60,dd);
        listTickets.add(a);
        return listTickets;
    }

}
