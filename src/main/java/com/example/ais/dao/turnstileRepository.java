package com.example.ais.dao;

import com.example.ais.model.turnstile;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

public class turnstileRepository {

    public static boolean IS_OPENED = true;
    public static boolean IS_WORKING = true;
    public static boolean IS_AVALIABLE = true;

    public static Map<Long, turnstile> turnstiles = new HashMap<>();

    private static AtomicInteger turnstileId = new AtomicInteger(0);

    public static int incrementAndGetId() {
        return turnstileId.incrementAndGet();
    }

    public static turnstile generateTurnstile(){
        int newKey = incrementAndGetId();
        turnstile Turnstile = new turnstile(newKey, IS_OPENED, IS_WORKING, IS_AVALIABLE);
        return Turnstile;
    }

    public static turnstile generateTurnstileWithException(){
        turnstile Turnstile = new turnstile(0, IS_OPENED, IS_WORKING, IS_AVALIABLE);
        return Turnstile;
    }
}