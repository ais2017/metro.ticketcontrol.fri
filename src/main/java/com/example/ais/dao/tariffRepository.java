package com.example.ais.dao;

import com.example.ais.model.tariff;
import com.example.ais.model.tariffCountCash;
import com.example.ais.model.tariffCountTrip;
import com.example.ais.model.tariffDelay;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class tariffRepository {

    public static tariffCountCash generateTariffCC(){
        tariffCountCash TariffCC = new tariffCountCash(1, 90);
        return TariffCC;
    }

    public static tariffCountCash generateTariffCCBPMN2(){

        tariffCountCash TariffCC = new tariffCountCash(3, 90);
        //TariffsСС.put(1, TariffCC);
        return TariffCC;
    }

    public static tariffCountCash generateTariffCCBPMN3(){

        tariffCountCash TariffCC = new tariffCountCash(5, 30);
        //TariffsСС.put(1, TariffCC);
        return TariffCC;
    }

    public static tariff generateTariffWithExceptionTariff1(){

        tariffCountCash TariffCC = new tariffCountCash(-1,2);
        return TariffCC;
    }

    public static tariff generateTariffWithExceptionTariff2(){

        tariffCountCash TariffCC = new tariffCountCash(2,-1);
        return TariffCC;
    }


    //============//
    //============//

    public static tariffCountTrip generateTariffCT(){


        Date dd = new Date(1016,12, 17, 17, 0);
        //tariff TariffForCT = new tariff("CT",7);

        tariffCountTrip TariffCT = new tariffCountTrip(2,1, dd);


        return TariffCT;
    }

    public static tariffCountTrip generateTariffCTWithExceptionTariff1(){

        Date dd = new Date(1016,12, 17, 17, 0);
        tariffCountTrip TariffCT = new tariffCountTrip(-10,1, dd);
        return TariffCT;
    }

    public static tariffCountTrip generateTariffCTWithExceptionTariff2(){

        Date dd = new Date(1016,12, 17, 17, 0);
        tariffCountTrip TariffCT = new tariffCountTrip(6, -1, dd);
        return TariffCT;
    }


    public static tariffDelay generateTariffD(){

        //tariff TariffForD = new tariff("D",10);
        Date dd = new Date(1016,12, 17, 17, 0);

        tariffDelay TariffD = new tariffDelay(10, 0, dd);

        return TariffD;
    }

    public static tariffDelay generateTariffDWithExceptionTariff1(){

        //tariff TariffForD = new tariff("D",8);

        Date dd = new Date(1016,12, 17, 17, 0);
        tariffDelay TariffD = new tariffDelay(8, -10, dd);
        return TariffD;
    }

    public static tariffDelay generateTariffDWithExceptionTariff2(){

        //tariff TariffForD = new tariff("D",8);

        tariffDelay TariffD = new tariffDelay(8, 5, null);
        return TariffD;
    }

}
