package com.example.ais;

import com.example.ais.repository.TariffRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@org.springframework.stereotype.Controller
public class Controller {

    @Autowired
    private TariffRepo tariffRepo;

    //@Autowired
    //private StoreRepository storeRepository;

    Date date1 = null;
    Date date2 = null;
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
    FileWriter writer = new FileWriter("/opt/scripts/page", false);

    public Controller() throws IOException {
    }

    @RequestMapping(value = "/page")
    public Model getForm(Model model) throws ParseException {
        Long start = System.currentTimeMillis();
        //Integer countAll = tariffRepo.countAll();
        //String countAllVal = countAll.toString();
        //model.addAttribute("countAll", countAll);

        Long end=System.currentTimeMillis() - start;
        String text=end.toString();
        try {
            Date date = Calendar.getInstance().getTime();
            writer.write("Datetime/Time/Count DB: "+date+","+text + ",");
            writer.append('\n');
            writer.flush();
        }
        catch(IOException ex){
            System.out.println(ex.getMessage());
        }

        return model;
    }
}