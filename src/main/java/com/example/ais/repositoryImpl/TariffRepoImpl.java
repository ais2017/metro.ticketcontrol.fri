package com.example.ais.repositoryImpl;

import com.example.ais.model.tariffCountCash;
import com.example.ais.model.tariffCountTrip;
import com.example.ais.model.tariffDelay;
import com.example.ais.repository.TariffRepo;

import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicInteger;


public class TariffRepoImpl implements TariffRepo {

    public static String ID_TARIFF_CC = "CountCash";
    public static String ID_TARIFF_CT = "CountTrip";
    public static String ID_TARIFF_D = "Delay";
    public static int ID_TICKET_CC = 1;
    public static int ID_TICKET_CT = 2;
    public static int ID_TICKET_D = 3;
    public static int COUNT_CC = 50;
    public static int COUNT_CT = 1;
    public static int COUNT_D = 0;
    public static Date VALID = new Date(1016,12, 17, 17, 0);
    private final static AtomicInteger idTicket = new AtomicInteger(0);
    public static Integer incrementAndGetId(){
        return idTicket.incrementAndGet();
    }
    private final static HashMap<Integer, AllTariff> storage = new HashMap<>();
//    private final static HashMap<Integer, tariffCountCash> storageCountCash = new HashMap<>();

    @Override
    public void create() {
        AllTariff newTariffCC = new AllTariff(ID_TARIFF_CC, ID_TICKET_CC, COUNT_CC, VALID);
        storage.put(incrementAndGetId(), newTariffCC);
        AllTariff newTariffCT = new AllTariff(ID_TARIFF_CT, ID_TICKET_CT, COUNT_CT, VALID);
        storage.put(incrementAndGetId(), newTariffCT);
        AllTariff newTariffD = new AllTariff(ID_TARIFF_D, ID_TICKET_D, COUNT_D, VALID);
        storage.put(incrementAndGetId(), newTariffD);
        //return newTariff;
    }

    @Override
    public void delete() {

    }

    @Override
    public AllTariff get(int idTicket) {
        return storage.get(idTicket);
    }

    @Override
    public tariffCountCash getCountCash(int idTicket) {
        return new tariffCountCash(storage.get(idTicket).getIdTicket(), storage.get(idTicket).getCount());
    }

    @Override
    public tariffCountTrip getCountTrip(int idTicket) {
        return new tariffCountTrip(storage.get(idTicket).getIdTicket(), storage.get(idTicket).getCount(), storage.get(idTicket).getValid());
    }

    @Override
    public tariffDelay getDelay(int idTicket) {
        return new tariffDelay(storage.get(idTicket).getIdTicket(), storage.get(idTicket).getCount(), storage.get(idTicket).getValid());
    }

    @Override
    public HashMap<Integer, AllTariff> getAll() {
        return storage;
    }

}
