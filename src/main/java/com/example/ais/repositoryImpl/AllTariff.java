package com.example.ais.repositoryImpl;

import java.util.Date;

public class AllTariff {

    private String idTariff;
    private int idTicket;
    private int count;
    private Date valid;

    public AllTariff(String idTariff, int idTicket, int count, Date valid) {
        if (count < 0
                || valid == null
                || idTariff == null
                || idTicket < 0) throw new IllegalArgumentException("Invalid params for 'AllTariff' class");
        this.idTariff = idTariff;
        this.idTicket = idTicket;
        this.count = count;
        this.valid = valid;
    }

    public String getIdTariff() {
        return idTariff;
    }

    public int getIdTicket() {
        return idTicket;
    }

    public int getCount() {
        return count;
    }

    public Date getValid() {
        return valid;
    }
}
