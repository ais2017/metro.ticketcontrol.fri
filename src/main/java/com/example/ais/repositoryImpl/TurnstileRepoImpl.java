package com.example.ais.repositoryImpl;

import com.example.ais.model.turnstile;
import com.example.ais.repository.TurnstileRepo;

import java.util.HashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class TurnstileRepoImpl implements TurnstileRepo {

    public static boolean IS_OPENED = false;
    public static boolean IS_WORKING = true;
    public static boolean IS_AVAILABLE = true;
    private final static AtomicInteger idTurnstile = new AtomicInteger(0);
    public static Integer incrementAndGetId(){
        return idTurnstile.incrementAndGet();
    }
    private final static HashMap<Integer, turnstile> storageTurnstile = new HashMap<>();

    @Override
    public turnstile create() {
        int i = incrementAndGetId();
        turnstile newTurnstile = new turnstile(i, IS_OPENED, IS_WORKING, IS_AVAILABLE);
        storageTurnstile.put(i, newTurnstile);

        return newTurnstile;
    }

    @Override
    public void delete() {

    }

    @Override
    public turnstile get(int id) {
        return storageTurnstile.get(id);
    }

    @Override
    public HashMap<Integer, turnstile> getAll() {
        return storageTurnstile;
    }
}
