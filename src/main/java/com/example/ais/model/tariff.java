package com.example.ais.model;

import java.sql.Date;

public abstract class tariff {

    private String idTariff;

    private int idTicket;

    public tariff(String idTariff, int idTicket){
        if ((idTariff == null)
                || (idTariff == "")
                || (idTicket <= 0)
                || !(idTariff instanceof String))
            throw new IllegalArgumentException("Invalid params for 'tariff' class");

        this.idTariff = idTariff;
        this.idTicket = idTicket;
    }

    public String getIdTariff() {
        return idTariff;
    }

    public int getIdTicket() {
        return idTicket;
    }

    public abstract boolean chargePass();

    public abstract boolean checkTicketActive(String idTrff, int idTckt);
}

