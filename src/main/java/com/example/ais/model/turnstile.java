package com.example.ais.model;


import org.apache.commons.lang.Validate;

public class turnstile {

    private int id;

    private boolean isOpened;

    private boolean isWorking;

    private boolean isAvaliable;

    public turnstile(int id, boolean isOpened, boolean isWorking, boolean isAvaliable){
        if (id == 0)  throw new IllegalArgumentException("Invalid params for 'turnstile' class");
            this.id = id;
            this.isOpened = isOpened;
            this.isWorking = isWorking;
            this.isAvaliable = isAvaliable;
    }

    public void doCloseTurnstile(){ //закрыть турникет
        this.isOpened = false;
        msgStoreboard("Турникет закрыт");
    }

    public void doOpenTurnstile(){ //открыть турникет
        this.isOpened = true;
        msgStoreboard("Турникет открыт");
    }

    public void doAvaliable(){ //турникет доступен
        this.isAvaliable = true;
        msgStoreboard("Турникет доступен");
    }

    public void doNotAvaliable(){ //турникет недоступен
        this.isAvaliable = false;
        msgStoreboard("Турникет недоступен");
    }

    public void fixCrash(){ // поломка
        this.isWorking = false;
        msgStoreboard("Турникет на ремонте");
    }

    public void doRepair(){ // починили
        this.isWorking = true;
        msgStoreboard("Турникет отремонтирован");
    }

//    public Long getId() {
//        return id;
//    }

    public boolean isAvaliable() {
        return isAvaliable;
    }

    public boolean isOpened() {
        return isOpened;
    }

    public boolean isWorking() {
        return isWorking;
    }

    public void msgStoreboard(String msg){
        System.out.println(msg);
    }
}