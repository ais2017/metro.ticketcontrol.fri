package com.example.ais.model;

import java.util.Date;

public class tariffDelay extends tariff {

    private int delay;

    private Date validThrough;

    public tariffDelay(int idTicket, int delay, Date validThrough){
        super("Delay", idTicket);
        if (validThrough == null
                || delay < 0) throw new IllegalArgumentException("Invalid params for 'tariffDelay' class");
        this.delay = delay;
        this.validThrough = validThrough;
    }

    public int getDelay() {
        return delay;
    }

    @Override
    public String getIdTariff() {
        return super.getIdTariff();
    }

    @Override
    public int getIdTicket() {
        return super.getIdTicket();
    }

    @Override
    public boolean chargePass(){

        if (getDelay() == 0) {
            this.delay = 5;
            return true;
        }
        return false;
    }
    @Override
    public boolean checkTicketActive(String idTrff, int idTckt){
        if (getIdTariff().equals(idTrff) && getIdTicket() == idTckt) {
            return true;
        }
        return false;
    }
}
