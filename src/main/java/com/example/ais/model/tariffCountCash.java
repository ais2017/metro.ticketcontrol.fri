package com.example.ais.model;

public class tariffCountCash extends tariff {

    private int countCash;

    public tariffCountCash(int idTicket, int countCash) {
        super("CountCash", idTicket);
        if (countCash < 0) throw new IllegalArgumentException("Invalid params for 'tariffCountCash' class");
        this.countCash = countCash;
    }

    public int getCountCash() {
        return countCash;
    }

    @Override
    public int getIdTicket() {
        return super.getIdTicket();
    }

    @Override
    public String getIdTariff() {
        return super.getIdTariff();
    }

    @Override
    public boolean chargePass(){

        if (getCountCash() >= 50) {
            this.countCash -= 50;
            return true;
        }
        return false;
    }

    @Override
    public boolean checkTicketActive(String idTrff, int idTckt){
        if (getIdTariff().equals(idTrff) && getIdTicket() == idTckt) {
            return true;
        }
        return false;
    }

}
