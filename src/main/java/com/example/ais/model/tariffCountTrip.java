package com.example.ais.model;

import java.util.Date;

public class tariffCountTrip extends tariff {

    private int countTrip;

    private Date validThrough;

    public tariffCountTrip(int idTicket, int countTrip, Date validThrough) {
        super("CountTrip", idTicket);
        if (countTrip < 0
                || validThrough == null) throw new IllegalArgumentException("Invalid params for 'tariffCountTrip' class");
        this.countTrip = countTrip;
        this.validThrough = validThrough;
    }

    public int getCountTrip() {
        return countTrip;
    }

    @Override
    public int getIdTicket() {
        return super.getIdTicket();
    }

    @Override
    public String getIdTariff() {
        return super.getIdTariff();
    }

    public Date getValidThrough() {
        return validThrough;
    }

    @Override
    public boolean chargePass(){

        if (getCountTrip() >= 1) {
            this.countTrip -= 1;
            return true;
        }
        return false;
    }

    @Override
    public boolean checkTicketActive(String idTrff, int idTckt){
        if (getIdTariff().equals(idTrff) && getIdTicket() == idTckt) {
            return true;
        }
        return false;
    }
}
