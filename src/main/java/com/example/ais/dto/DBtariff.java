package com.example.ais.dto;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name="tariff")
public class DBtariff extends TransactionalEntity {

    private String idTariff;
    private String idTicket;
    private Integer count;
    private Date valid;

    public DBtariff() {
    }

    public Date getValid() {
        return valid;
    }
    public String getIdTariff() {
        return idTariff;
    }
    @Override
    public Integer getId() {
        return super.getId();
    }
    @Override
    public void setId(Integer id) {
        super.setId(id);
    }
    public Integer getCount() {
        return count;
    }
    public String getIdTicket() {
        return idTicket;
    }
    public void setCount(Integer count) {
        this.count = count;
    }
    public void setIdTariff(String idTariff) {
        this.idTariff = idTariff;
    }
    public void setIdTicket(String idTicket) {
        this.idTicket = idTicket;
    }
    public void setValid(Date valid) {
        this.valid = valid;
    }

}