package com.example.ais.service;

import com.example.ais.model.turnstile;
import com.example.ais.repository.TurnstileRepo;
import org.springframework.stereotype.Service;

import java.util.HashMap;

@Service
public class TurnstileService {

    private TurnstileRepo turnstileRepo;

    public HashMap<Integer, turnstile> getAll(){
        return turnstileRepo.getAll();
    }

    public TurnstileService (TurnstileRepo turnstileRepo) {
        this.turnstileRepo = turnstileRepo;
    }

    public void createTurnstile (){};
}