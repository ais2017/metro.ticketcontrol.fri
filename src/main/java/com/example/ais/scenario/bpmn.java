//package com.example.ais.scenario;
//
//import com.example.ais.dao.forBPMNRepository;
//import com.example.ais.dao.tariffRepository;
//import com.example.ais.dao.turnstileRepository;
//import com.example.ais.model.*;
//import org.junit.jupiter.api.Test;
//
//import java.util.ArrayList;
//import java.util.Collection;
//import java.util.Date;
//
//public class bpmn {
//
//    //interface tickets 2 meth: получить билет по id, записать
//    // interface turnstiles 2 meth
//    @Test
//    public void tst(){
//        bmpnBK("CountCash",1, 1, null);
//    }
//
//    @Test
//    public void bmpnBK(String idTariff, int idTicket, int count, Date validate){ //idTutnstile & idTicket
//
//        ArrayList<forBPMNRepository> ticketlist = forBPMNRepository.generateBPMNList();
//        forBPMNRepository a = ticketlist.get(0);
//
//        //tariffCountCash TariffCC1 = tariffRepository.generateTariffCC(); // то, что вводит пользователь
//
//        turnstile Turnstile = turnstileRepository.generateTurnstile();
//
//        //tariffCountCash TariffCC1 = new tariffCountCash(idTicket, count);
//
//        if (idTariff.equals("CountCash")) {
//            tariffCountCash TariffCC1 = new tariffCountCash(idTicket, count);
//            // считываем данные билета
//            boolean checkTariff = false;
//
//            for (int i = 0; i < ticketlist.size(); i++){
//                a = ticketlist.get(i);
//                checkTariff = TariffCC1.checkTicketActive(a.getTariff(), a.getIdTicket());
//                if (checkTariff) // если такой билет существует
//                    break;
//            }
//
//            if (!checkTariff){
//                Turnstile.msgStoreboard("Билет не найден в базе");
//                //Turnstile.doCloseTurnstile(); // если такого билета нет, то турникет закрыт
//                return;
//            }
//
//            // если есть такой билет:
//
//            // попытка вычесть поездку
//            boolean boolTariffChargePass = TariffCC1.chargePass();
//            // попытка прошла успешно:
//            if (boolTariffChargePass){
//                Turnstile.doOpenTurnstile();
//                Turnstile.doCloseTurnstile();
//            } else {
//                Turnstile.msgStoreboard("Недостаточно средств");
//            }
//
//            Turnstile.msgStoreboard("");
//        }
//
//        if (idTariff.equals("CountTrip")) {
//            tariffCountTrip TariffCC1 = new tariffCountTrip(idTicket, count, validate);
//            // считываем данные билета
//            boolean checkTariff = false;
//
//            for (int i = 0; i < ticketlist.size(); i++){
//                a = ticketlist.get(i);
//                checkTariff = TariffCC1.checkTicketActive(a.getTariff(), a.getIdTicket());
//                if (checkTariff) // если такой билет существует
//                    break;
//            }
//
//            if (!checkTariff){
//                Turnstile.msgStoreboard("Билет не найден в базе");
//                //Turnstile.doCloseTurnstile(); // если такого билета нет, то турникет закрыт
//                return;
//            }
//
//            // если есть такой билет:
//
//            // попытка вычесть поездку
//            boolean boolTariffChargePass = TariffCC1.chargePass();
//            // попытка прошла успешно:
//            if (boolTariffChargePass){
//                Turnstile.doOpenTurnstile();
//                Turnstile.doCloseTurnstile();
//            } else {
//                Turnstile.msgStoreboard("Недостаточно средств");
//            }
//
//            Turnstile.msgStoreboard("");
//        }
//
//        if (idTariff.equals("Delay")) {
//            tariffDelay TariffCC1 = new tariffDelay(idTicket, count, validate);
//            // считываем данные билета
//            boolean checkTariff = false;
//
//            for (int i = 0; i < ticketlist.size(); i++){
//                a = ticketlist.get(i);
//                checkTariff = TariffCC1.checkTicketActive(a.getTariff(), a.getIdTicket());
//                if (checkTariff) // если такой билет существует
//                    break;
//            }
//
//            if (!checkTariff){
//                Turnstile.msgStoreboard("Билет не найден в базе");
//                //Turnstile.doCloseTurnstile(); // если такого билета нет, то турникет закрыт
//                return;
//            }
//
//            // если есть такой билет:
//
//            // попытка вычесть поездку
//            boolean boolTariffChargePass = TariffCC1.chargePass();
//            // попытка прошла успешно:
//            if (boolTariffChargePass){
//                Turnstile.doOpenTurnstile();
//                Turnstile.doCloseTurnstile();
//            } else {
//                Turnstile.msgStoreboard("Недостаточно средств");
//            }
//
//            Turnstile.msgStoreboard("");
//        }
//    }
//
//}
