package com.example.ais;

import com.example.ais.model.tariffCountCash;
import com.example.ais.model.tariffCountTrip;
import com.example.ais.model.tariffDelay;
import com.example.ais.model.turnstile;
import com.example.ais.repositoryImpl.AllTariff;
import com.example.ais.repositoryImpl.TariffRepoImpl;
import com.example.ais.repositoryImpl.TurnstileRepoImpl;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

//import com.example.ais.service.TariffCountCashService;

public class TariffTestBK {

    TariffRepoImpl allTariffRepo = new TariffRepoImpl();
    TurnstileRepoImpl turnstileRepo = new TurnstileRepoImpl();

    @Test
    public void start() {
        //AllTariff allTariff =
        allTariffRepo.create();
        //turnstile turnstile =
        turnstileRepo.create();
        BK(1);
        BK(2);
        BK(3);
        BK(4);
    }

    public void BK(int idTicket) {

        turnstile turnstile = turnstileRepo.get(1);

        HashMap<Integer, AllTariff> repo = allTariffRepo.getAll();
        // Получаем набор элементов
        Set<Map.Entry<Integer, AllTariff>> set = repo.entrySet();

        // Отобразим набор
//        for (Map.Entry<Integer, AllTariff> me : set) {
//            System.out.print(me.getKey() + ": ");
//            System.out.println(me.getValue().getIdTariff());
//        }

        String Tariff = null;
        try {

            Tariff = allTariffRepo.get(idTicket).getIdTariff(); // поиск по репозиторию

        boolean checkTariff = false;
        switch (Tariff){
            case "CountCash":
                tariffCountCash tariffCountCash = allTariffRepo.getCountCash(idTicket);
                    // попытка вычесть поездку
                    boolean boolTariffChargePass = tariffCountCash.chargePass();
                    // попытка прошла успешно:
                    if (boolTariffChargePass) {
                        turnstile.doOpenTurnstile();
                        turnstile.doCloseTurnstile();
                    } else {
                        turnstile.msgStoreboard("Недостаточно средств");
                    }

                break;

            case "CountTrip":
            tariffCountTrip tariffCountTrip = allTariffRepo.getCountTrip(idTicket);
                // попытка вычесть поездку
                boolTariffChargePass = tariffCountTrip.chargePass();
                // попытка прошла успешно:
                if (boolTariffChargePass) {
                    turnstile.doOpenTurnstile();
                    turnstile.doCloseTurnstile();
                } else {
                    turnstile.msgStoreboard("Недостаточное количество поездок");
                }
            break;
            case "Delay":
                tariffDelay tariffDelay = allTariffRepo.getDelay(idTicket);
                    // попытка вычесть поездку
                    boolTariffChargePass = tariffDelay.chargePass();
                    // попытка прошла успешно:
                    if (boolTariffChargePass) {
                        turnstile.doOpenTurnstile();
                        turnstile.doCloseTurnstile();
                    } else {
                        turnstile.msgStoreboard("Задержка. Попробуйте через несколько минут");
                    }
                break;
        }

        } catch (Exception ex){
            System.out.println("Билет не найден в базе");
        }

            turnstile.msgStoreboard("");
        }

    }
