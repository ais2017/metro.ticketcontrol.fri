package com.example.ais;
import com.example.ais.dao.turnstileRepository;

import com.example.ais.model.turnstile;
import org.junit.Assert;
//import org.junit.jupiter.api.Test;
import org.junit.Test;


public class turnstileTest {
    @Test
    public void test(){
        turnstile Turnstile = turnstileRepository.generateTurnstile();

        Assert.assertEquals(Turnstile.isOpened(), true);
        Turnstile.doCloseTurnstile();
        Assert.assertEquals(Turnstile.isOpened(), false);
        Turnstile.doOpenTurnstile();
        Assert.assertEquals(Turnstile.isOpened(), true);

        Assert.assertEquals(Turnstile.isAvaliable(), true);
        Turnstile.doNotAvaliable();
        Assert.assertEquals(Turnstile.isAvaliable(), false);
        Turnstile.doAvaliable();
        Assert.assertEquals(Turnstile.isAvaliable(), true);

        Assert.assertEquals(Turnstile.isWorking(), true);
        Turnstile.fixCrash();
        Assert.assertEquals(Turnstile.isWorking(), false);
        Turnstile.doRepair();
        Assert.assertEquals(Turnstile.isWorking(), true);



    }

    @Test(expected = IllegalArgumentException.class)
    public void testWithException() {
        turnstile Turnstile = turnstileRepository.generateTurnstileWithException();

    }
}
