package com.example.ais;

import com.example.ais.dao.tariffRepository;
import com.example.ais.model.tariff;
import com.example.ais.model.tariffCountCash;
import com.example.ais.model.tariffCountTrip;
import com.example.ais.model.tariffDelay;
import org.junit.Assert;
import org.junit.Test;



public class tariffTest {
    @Test
    public void testCCOkey() {

        tariffCountCash TariffCC = tariffRepository.generateTariffCC();

        boolean checkTariff = TariffCC.checkTicketActive("CountCash", 1);

        //boolean checkTariff = checkTicketActive(TariffCC, "CountCash", 1);
        Assert.assertEquals(checkTariff,true);

        checkTariff = TariffCC.checkTicketActive("CC", 1);
        //checkTariff = checkTicketActive(TariffCC, "CC", 1);
        Assert.assertEquals(checkTariff,false);

        boolean boolTariffChargePass = TariffCC.chargePass();
        Assert.assertEquals(boolTariffChargePass, true);

        boolTariffChargePass = TariffCC.chargePass();
        Assert.assertEquals(boolTariffChargePass, false);

    }



    @Test(expected = IllegalArgumentException.class)
    public void testWithExceptionTariff1() {
        tariff Tariff = tariffRepository.generateTariffWithExceptionTariff1();
    }


    @Test(expected = IllegalArgumentException.class)
    public void testWithExceptionTariff2() {
        tariff Tariff = tariffRepository.generateTariffWithExceptionTariff2();
    }


    @Test
    public void testCTOkey(){

        tariffCountTrip TariffCT = tariffRepository.generateTariffCT();

        boolean checkTariff = TariffCT.checkTicketActive("CountTrip",2);
        Assert.assertEquals(checkTariff,true);

        checkTariff = TariffCT.checkTicketActive("CountTrip", 6);
        Assert.assertEquals(checkTariff,false);

        checkTariff = TariffCT.checkTicketActive("CT", 7);
        Assert.assertEquals(checkTariff,false);

        //charge pass

        boolean boolTariffChargePass = TariffCT.chargePass();
        Assert.assertEquals(boolTariffChargePass, true);

        boolTariffChargePass = TariffCT.chargePass();
        Assert.assertEquals(boolTariffChargePass, false);

    }

    @Test(expected = IllegalArgumentException.class)
    public void testWithExceptionTariffCT1() {
        tariff TariffCT = tariffRepository.generateTariffCTWithExceptionTariff1();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testWithExceptionTariffCT2() {
        tariff TariffCT = tariffRepository.generateTariffCTWithExceptionTariff2();
    }

    @Test
    public void testDOkey(){

        tariffDelay TariffD = tariffRepository.generateTariffD();

        boolean checkTariff = TariffD.checkTicketActive("Delay", 10);
        Assert.assertEquals(checkTariff,true);

        checkTariff = TariffD.checkTicketActive("CountTrip", 10);
        Assert.assertEquals(checkTariff,false);

        checkTariff = TariffD.checkTicketActive("Delay", 7);
        Assert.assertEquals(checkTariff,false);

        //charge pass

        boolean boolTariffChargePass = TariffD.chargePass();
        Assert.assertEquals(boolTariffChargePass, true);

        boolTariffChargePass = TariffD.chargePass();
        Assert.assertEquals(boolTariffChargePass, false);



    }

    @Test(expected = IllegalArgumentException.class)
    public void testWithExceptionTariffD1() {
        tariff TariffD = tariffRepository.generateTariffDWithExceptionTariff1();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testWithExceptionTariffD2() {
        tariff TariffD = tariffRepository.generateTariffDWithExceptionTariff2();
    }

}


